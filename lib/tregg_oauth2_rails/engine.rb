module TreggOauth2Rails
  class Engine < ::Rails::Engine
    isolate_namespace TreggOauth2Rails
  end
end
