require 'rails-api/action_controller/api'
require 'active_model_serializers'
require 'songkick/oauth2/provider'
require 'koala'
require 'tregg_oauth2_rails/engine'

module TreggOauth2Rails
  AUTH_ERROR_RESPONSE = {'error' => 'Authentication error. Please identify yourself'}
  mattr_accessor :authenticable_class

  def self.authenticable_class
    @@authenticable_class.constantize
  end

  def self.authenticable_table
    @@authenticable_class.tableize
  end

  def self.access_token(resource_owner, scopes, env)
    Songkick::OAuth2::Provider.access_token(resource_owner, scopes, env)
  end
end
