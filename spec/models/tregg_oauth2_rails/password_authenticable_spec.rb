require 'spec_helper'
# NOTE: user is a User defined by the dummy app

describe TreggOauth2Rails::PasswordAuthenticable do
  describe '#recover_password_token!' do
    subject { @user.recover_password_token! }

    it { should == @user.reload.reset_pwd_token }

    context 'when a unique token cannot be found' do
      before do
        SecureRandom.stub(:urlsafe_base64).and_return(24)
        foo_user = create(:user, :username => 'bar@foo.com')
        foo_user.recover_password_token!
      end

      it 'fails' do
        expect { @user.recover_password_token! }.to raise_error(ActiveRecord::RecordNotUnique)
      end
    end
  end

  describe '.by_recovery_token' do
    before { @rtoken = @user.recover_password_token! }

    subject { User.by_recovery_token(@rtoken) }

    it { should == @user.reload }
  end

  describe '#password_authenticate?' do
    subject { @user.password_authenticate?('password') }

    it { should be_true }

    context 'with the wrong password' do
      subject { @user.password_authenticate?('foo') }

      it { should be_false }
    end

    context 'with a blank password' do
      context 'when nil' do
        before { @user.send(:write_attribute, :password, nil) }
        subject { @user.password_authenticate?('password') }

        it { should be_false }
      end

      context 'when empty string' do
        before { @user.send(:write_attribute, :password, '') }
        subject { @user.password_authenticate?('password') }

        it { should be_false }
      end
    end
  end

  describe '#password=' do
    context 'with a nil password' do
      it 'fails' do
        expect { @user.password = nil }.to raise_error
      end
    end

    context 'with an empty password' do
      it 'fails' do
        expect { @user.password = '' }.to raise_error
      end
    end

    context 'with all-spaces password' do
      it 'fails' do
        expect { @user.password = 10.times.collect { ' ' }.join }.to raise_error
      end
    end

    context "with a #{TreggOauth2Rails::PasswordAuthenticable::MIN_PASSWORD_LENGTH - 1} chars password" do
      it 'fails' do
        expect { @user.password = (TreggOauth2Rails::PasswordAuthenticable::MIN_PASSWORD_LENGTH - 1).times.collect { 'a' }.join }.to raise_error
      end
    end
  end
end
