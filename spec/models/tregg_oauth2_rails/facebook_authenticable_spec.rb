require 'spec_helper'
require 'koala'
# NOTE: user is a User defined by the dummy app

describe TreggOauth2Rails::FacebookAuthenticable do
  before(:all) do
    @fb_test_users = Koala::Facebook::TestUsers.new(:app_id => "197838773569384", :secret => "8c292e1404cd46d2772841061d863934")
    @fb_user = @fb_test_users.create(true, "publish_stream,offline_access,email")
  end

  after(:all) { @fb_test_users.delete(@fb_user) }

  describe '.from_facebook' do
    subject { User.from_facebook(@fb_user['access_token']).facebook_uid }
    it { should == @fb_user['id'] }
  end

  describe '#facebook_connect' do
    subject { @user.facebook_connect(@fb_user['access_token']).facebook_uid }
    it { should == @fb_user['id'] }
  end

  describe '#facebook_data=' do
    before(:all) do
      @me = TreggOauth2Rails::FacebookUser.new(@fb_user['access_token']).me
      @user.facebook_data=@me
    end

    describe 'id' do
      subject { @user.facebook_uid }
      it { should == @me['id'] }
    end

    describe 'first_name' do
      subject { @user.facebook_first_name }
      it { should == @me['first_name'] }
    end

    describe 'last_name' do
      subject { @user.facebook_last_name }
      it { should == @me['last_name'] }
    end

    describe 'gender' do
      subject { @user.facebook_gender }
      it { should == @me['gender'] }
    end

    describe 'username' do
      subject { @user.facebook_username }
      it { should == @me['username'] }
    end

    describe 'email' do
      subject { @user.facebook_email }
      it { should == @me['email'] }
    end

    describe 'picture' do
      subject { @user.facebook_picture }
      it { should_not be_nil }
    end
  end
end
