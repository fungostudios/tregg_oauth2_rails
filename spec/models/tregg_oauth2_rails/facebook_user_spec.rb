require 'spec_helper'

describe TreggOauth2Rails::FacebookUser do
  before(:all) do
    @fb_test_users = Koala::Facebook::TestUsers.new(:app_id => "197838773569384", :secret => "8c292e1404cd46d2772841061d863934")
    @fb_user = @fb_test_users.create(true, "publish_stream,offline_access,email")
  end

  after(:all) { @fb_test_users.delete(@fb_user) }

  describe '#me' do
    describe 'id' do
      subject { TreggOauth2Rails::FacebookUser.new(@fb_user['access_token']).me['id'] }
      it { should == @fb_user['id'] }
    end
  end
end
