require 'spec_helper'
require 'rspec_api_documentation/dsl'

resource 'Authenticables' do
  post '/oauth/authorize' do
    parameter :client_id, 'Tregg app ID'
    parameter :grant_type, 'Authentication strategy'
    parameter :assertion_type, 'The type of the assertion strategy'
    parameter :assertion, 'Assertion value'
    parameter :username, 'Username for password authentication'
    parameter :password, 'The password'

    required_parameters :client_id, :grant_type

    before do
      @user = create(:user, :password => 'secret')
      @password_params = {
        :grant_type => 'password',
        :client_id => @tregg_client.client_id,
        :username => @user.username,
        :password => 'secret'
      }
    end

    before(:all) do
      @test_users = Koala::Facebook::TestUsers.new(:app_id => "197838773569384", :secret => "8c292e1404cd46d2772841061d863934")
      @fb_user = @test_users.create(true, "publish_stream,offline_access,email")
      @fb_params = {
        :grant_type => 'assertion',
        :client_id => @tregg_client.client_id,
        :assertion_type => 'https://graph.facebook.com',
        :assertion => @fb_user['access_token']
      }
    end

    after(:all) do
      @test_users.delete(@fb_user['id'])
    end

    example 'Password authentication' do
      do_request(@password_params)
      status.should == 200
    end

    example 'Facebook authentication' do
      do_request(@fb_params)
      status.should == 200
    end
  end

  post '/sign-up' do
    parameter :client_id, 'Tregg app ID'
    parameter :username, 'The username for the new user'
    parameter :password, 'The password for the new user'

    required_parameters :client_id, :username, :password

    before do
      @params = {
        :client_id => @tregg_client.client_id,
        :username => 'foo',
        :password => 'foobar',
      }
    end

    example 'Sign-up a new user' do
      do_request(@params)
      status.should == 200
    end

    example 'Sing-up a new user with an already taken username' do
      do_request(@params.merge(:username => @user.username))
      status.should == 412
    end
  end

  post '/sign-out' do
    header 'Authorization', 'OAuth your-access-token'

    example 'Sign-out the currently signed-in user' do
      a_user = create(:user)
      a_token = Songkick::OAuth2::Model::Authorization.for(a_user, @tregg_client).generate_access_token
      TreggOauth2Rails::ApplicationController.any_instance.stub(:authenticate!).and_return(true)
      TreggOauth2Rails::ApplicationController.any_instance.stub(:current_user).and_return(a_user)
      Songkick::OAuth2::Provider::AccessToken.any_instance.stub(:authorization).and_return(Songkick::OAuth2::Model::Authorization.last)
      do_request
      status.should == 200
    end
  end
end
