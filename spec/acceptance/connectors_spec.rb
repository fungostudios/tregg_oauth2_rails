require 'spec_helper'
require 'rspec_api_documentation/dsl'

resource 'Connectors' do
  post '/me/facebook_connect' do
    header 'Authorization', 'OAuth your-access-token'

    parameter :facebook_token, 'The user\'s Facebook access token'
    required_parameters :facebook_token

    before(:all) do
      @test_users = Koala::Facebook::TestUsers.new(:app_id => "197838773569384", :secret => "8c292e1404cd46d2772841061d863934")
      @fb_user = @test_users.create(true, "publish_stream,offline_access,email")
    end

    after(:all) do
      @test_users.delete(@fb_user['id'])
    end

    example 'Link a Facebook account' do
      a_user = create(:user)
      a_token = Songkick::OAuth2::Model::Authorization.for(a_user, @tregg_client).generate_access_token
      TreggOauth2Rails::ApplicationController.any_instance.stub(:authenticate!).and_return(true)
      TreggOauth2Rails::ApplicationController.any_instance.stub(:current_user).and_return(a_user)
      Songkick::OAuth2::Provider::AccessToken.any_instance.stub(:authorization).and_return(Songkick::OAuth2::Model::Authorization.last)
      do_request({:facebook_token => @fb_user['access_token']})
      status.should == 200
    end
  end
end
