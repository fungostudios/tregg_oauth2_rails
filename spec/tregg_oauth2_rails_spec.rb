require 'spec_helper'

describe TreggOauth2Rails do
  describe 'error message' do
    subject { TreggOauth2Rails::AUTH_ERROR_RESPONSE }
    it { should_not be_nil }
  end

  describe '.authenticable_class' do
    before { TreggOauth2Rails.authenticable_class = 'User' }

    subject { TreggOauth2Rails.authenticable_class }
    it { should == User }
  end

  describe '.authenticable_table' do
    before { TreggOauth2Rails.authenticable_class = 'User' }

    subject { TreggOauth2Rails.authenticable_table }
    it { should == 'users' }
  end
end
