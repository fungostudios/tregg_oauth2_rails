require 'spec_helper'

describe User do
  describe 'is authenticable' do
    it 'responds to grant_access!' do
      User.new.respond_to?(:grant_access!).should be_true
    end

    it 'responds to oauth2_authorization_for' do
      User.new.respond_to?(:oauth2_authorization_for).should be_true
    end
  end
end
