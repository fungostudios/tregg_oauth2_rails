# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user, aliases: [:oauth2_resource_owner] do
    sequence(:username) {|n| "user_#{n}"}
    password 'password'
    sequence(:reset_pwd_token) {|n| "token_#{n}"}
    password_reset_sent_at Time.now
    role 'admin'
  end

  factory :oauth2_client, class: Songkick::OAuth2::Model::Client, aliases: [:client] do
    name "BTask"
    redirect_uri "http://btask.cloudapp.net"
  end

  factory :oauth2_authorization, class: Songkick::OAuth2::Model::Authorization do
    oauth2_resource_owner
    client
    sequence(:access_token) {|n| "foooo #{n}"}
  end
end
