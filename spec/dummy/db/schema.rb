# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131108152757) do

  create_table "oauth2_authorizations", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "oauth2_resource_owner_type"
    t.integer  "oauth2_resource_owner_id"
    t.integer  "client_id"
    t.string   "scope"
    t.string   "code",                       limit: 40
    t.string   "access_token_hash",          limit: 40
    t.string   "refresh_token_hash",         limit: 40
    t.datetime "expires_at"
  end

  add_index "oauth2_authorizations", ["access_token_hash"], name: "index_oauth2_authorizations_on_access_token_hash", unique: true
  add_index "oauth2_authorizations", ["client_id", "code"], name: "index_oauth2_authorizations_on_client_id_and_code", unique: true
  add_index "oauth2_authorizations", ["client_id", "oauth2_resource_owner_type", "oauth2_resource_owner_id"], name: "index_owner_client_pairs", unique: true
  add_index "oauth2_authorizations", ["client_id", "refresh_token_hash"], name: "idx_auth_client_refresh_hash", unique: true

  create_table "oauth2_clients", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "oauth2_client_owner_type"
    t.integer  "oauth2_client_owner_id"
    t.string   "name"
    t.string   "client_id"
    t.string   "client_secret_hash"
    t.string   "redirect_uri"
  end

  add_index "oauth2_clients", ["client_id"], name: "index_oauth2_clients_on_client_id", unique: true
  add_index "oauth2_clients", ["name"], name: "index_oauth2_clients_on_name", unique: true

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "password"
    t.string   "reset_pwd_token"
    t.datetime "password_reset_sent_at"
    t.string   "facebook_uid"
    t.string   "facebook_first_name"
    t.string   "facebook_last_name"
    t.string   "facebook_username"
    t.string   "facebook_picture"
    t.string   "facebook_gender"
    t.datetime "facebook_birthday"
    t.string   "role"
    t.string   "facebook_email"
  end

  add_index "users", ["facebook_uid"], name: "index_users_on_facebook_uid", unique: true
  add_index "users", ["reset_pwd_token"], name: "index_users_on_reset_pwd_token", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
