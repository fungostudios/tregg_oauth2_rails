# This migration comes from tregg_oauth2_rails (originally 20131108152633)
class AddFacebookEmailToAuthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    add_column table, :facebook_email, :string
  end
end
