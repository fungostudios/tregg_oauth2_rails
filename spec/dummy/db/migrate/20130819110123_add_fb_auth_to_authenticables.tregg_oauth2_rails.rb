# This migration comes from tregg_oauth2_rails (originally 20130819090421)
class AddFbAuthToAuthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    add_column table, :facebook_uid, :string
    add_column table, :facebook_first_name, :string
    add_column table, :facebook_last_name, :string
    add_column table, :facebook_username, :string
    add_column table, :facebook_picture, :string
    add_column table, :facebook_gender, :string
    add_column table, :facebook_birthday, :timestamp

    add_index table, :facebook_uid, :unique => true
  end
end
