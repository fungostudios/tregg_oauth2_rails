# This migration comes from tregg_oauth2_rails (originally 20130828135738)
class AddRoleToAnthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    add_column table, :role, :string
  end
end
