# This migration comes from tregg_oauth2_rails (originally 20130814135534)
class CreateAuthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    if ! ActiveRecord::Base.connection.table_exists?(table)
      create_table table do |t|
        t.timestamps
      end
    end

    Songkick::OAuth2::Model::Schema.migrate
  end
end
