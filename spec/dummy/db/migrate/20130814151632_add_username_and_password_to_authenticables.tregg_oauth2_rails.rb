# This migration comes from tregg_oauth2_rails (originally 20130814140254)
class AddUsernameAndPasswordToAuthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    add_column table, :username, :string
    add_column table, :password, :string
    add_column table, :reset_pwd_token, :string
    add_column table, :password_reset_sent_at, :timestamp

    add_index table, :username, :unique => true
    add_index table, :reset_pwd_token, :unique => true
  end
end
