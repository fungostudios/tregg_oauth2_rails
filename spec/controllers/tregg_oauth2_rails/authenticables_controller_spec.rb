require 'spec_helper'
require 'koala'

describe TreggOauth2Rails::AuthenticablesController do
  describe 'POST /oauth/authorize' do
    context 'Password Authentication' do
      before do
        @user = create(:user)
        @params = {
          :grant_type => 'password',
          :client_id => @tregg_client.client_id,
          :username => @user.username
        }
      end

      context 'with wrong password' do
        it 'fails' do
          post :create, @params.merge({:password => 'foo', :use_route => :tregg_oauth2_rails})
          response.status.should == 400
        end
      end

      context 'with correct password' do
        it 'is OK' do
          post :create, @params.merge({:password => 'password', :use_route => :tregg_oauth2_rails})
          response.status.should == 200
        end
      end

      context 'with a non-existing user' do
        it 'fails' do
          post :create, @params.merge({:username => 'unknown', :password => 'password', :use_route => :tregg_oauth2_rails})
          response.status.should == 400
        end
      end
    end

    context 'Facebook Authentication' do
      before(:all) do
        @test_users = Koala::Facebook::TestUsers.new(:app_id => "197838773569384", :secret => "8c292e1404cd46d2772841061d863934")
        @fb_user = @test_users.create(true, "publish_stream,offline_access,email")
        @params = {
          :grant_type => 'assertion',
          :client_id => @tregg_client.client_id,
          :assertion_type => 'https://graph.facebook.com',
          :assertion => @fb_user['access_token']
        }
      end

      after(:all) do
        @test_users.delete(@fb_user['id'])
      end

      it 'registers and authenticates a new Facebook user' do
        post :create, @params.merge({:use_route => :tregg_oauth2_rails})

        response.status.should == 200
        body = JSON.parse(response.body)
        body['access_token'].should_not be_nil

        User.last.facebook_uid.should == @fb_user['id']
      end

      it 'fails to authenticate a user with an invalid Facebook token' do
        @test_users.update(@fb_user, {:password => 'foo12345ciao'})
        post :create, @params.merge({:use_route => :tregg_oauth2_rails})
        response.status.should == 400
        body = JSON.parse(response.body)
        body['error'].should_not be_nil
      end
    end
  end

  describe 'POST /sign-up' do
    before do
      @params = {
        :client_id => @tregg_client.client_id,
        :username => 'foo',
        :password => 'foobar',
        :use_route => :tregg_oauth2_rails
      }
    end

    describe 'required params' do
      context 'whitout client_id' do
        it 'fails' do
          post :sign_up, @params.reject {|param, value| param == :client_id}
          response.status.should == 400
        end
      end

      context 'whitout username' do
        it 'fails' do
          post :sign_up, @params.reject {|param, value| param == :username}
          response.status.should == 400
        end
      end

      context 'whitout password' do
        it 'fails' do
          post :sign_up, @params.reject {|param, value| param == :password}
          response.status.should == 400
        end
      end
    end

    describe 'with an invalid client_id' do
      it 'fails' do
        post :sign_up, @params.merge(:client_id => 0)
        response.status.should == 400
      end
    end

    it 'register a new user' do
      post :sign_up, @params
      response.status.should == 200
      body = JSON.parse(response.body)
      body['user'].should_not be_nil
      body['access_token'].should_not be_nil
    end

    describe 'with an already taken username' do
      before do
        @params = {
          :client_id => @tregg_client.client_id,
          :username => @user.username,
          :password => 'foobar',
          :use_route => :tregg_oauth2_rails
        }
      end

      it 'fails' do
        post :sign_up, @params
        response.status.should == 412
      end

      it 'warns that the username is already in use' do
        post :sign_up, @params
        body = JSON.parse(response.body)
        body['error'].should == 'Username already in use'
      end
    end
  end

  describe 'POST /sign-out' do
    before do
      @a_user = create(:user)
      @a_token = Songkick::OAuth2::Model::Authorization.for(@a_user, @tregg_client).generate_access_token
      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@a_token}"
    end

    it 'is ok' do
      post :sign_out, {:use_route => :tregg_oauth2_rails}
      response.status.should == 200
    end

    it 'signs out the current_user' do
      post :sign_out, {:use_route => :tregg_oauth2_rails}
      response.status.should == 200

      get :show, {:use_route => :tregg_oauth2_rails}
      response.status.should == 401
    end

    it 'is authenticated' do
      request.env['HTTP_AUTHORIZATION'] = "OAuth foo"
      post :sign_out, {:use_route => :tregg_oauth2_rails}
      response.status.should == 401
    end
  end

  describe 'GET /me' do
    context 'with valid token' do
      before { request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}" }

      it 'is OK' do
        get :show, {:use_route => :tregg_oauth2_rails}
        response.status.should == 200
      end

      it 'returns user and role' do
        get :show, {:use_route => :tregg_oauth2_rails}
        response.body.should == {:user => @user, :role => @user.role}.to_json
      end
    end

    context 'with invalid token' do
      before { request.env['HTTP_AUTHORIZATION'] = 'invalid token' }

      it 'is unauthorized' do
        get :show, {:use_route => :tregg_oauth2_rails}
        response.status.should == 401
      end
    end
  end
end
