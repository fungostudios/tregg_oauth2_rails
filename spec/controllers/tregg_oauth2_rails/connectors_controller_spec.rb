require 'spec_helper'

describe TreggOauth2Rails::ConnectorsController do
  describe 'POST /me/facebook_connect' do
    before(:all) do
      @test_users = Koala::Facebook::TestUsers.new(:app_id => '197838773569384', :secret => '8c292e1404cd46d2772841061d863934')
      @fb_user = @test_users.create(true, 'publish_stream,offline_access,email')
      @params = {
        :grant_type => 'assertion',
        :client_id => @tregg_client.client_id,
        :assertion_type => 'https://graph.facebook.com',
        :assertion => @fb_user['access_token']
      }
    end

    after(:all) do
      @test_users.delete(@fb_user['id'])
    end

    it 'requires authentication' do
      post :facebook_connect, :use_route => :tregg_oauth2_rails
      response.status.should == 401
    end

    it 'adds a FB account for @user' do
      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}"
      post :facebook_connect, {:facebook_token => @fb_user['access_token'], :use_route => :tregg_oauth2_rails}
      response.status.should == 200

      @user.reload
      @user.facebook_uid.should == @fb_user['id']
    end

    it 'does not change the Facebook account' do
      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}"
      post :facebook_connect, {:facebook_token => @fb_user['access_token'], :use_route => :tregg_oauth2_rails}

      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}"
      post :facebook_connect, {:facebook_token => @fb_user['access_token'], :use_route => :tregg_oauth2_rails}

      response.status.should == 400
      JSON.parse(response.body)['error'].should == 'Already connected to Facebook'
    end

    it 'requires a Facebook access token to link a Facebook account' do
      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}"
      post :facebook_connect, :use_route => :tregg_oauth2_rails
      response.status.should == 400
      JSON.parse(response.body)['error'].should == 'param not found: facebook_token'
    end

    it 'does not connet with Facebook with an invalid token' do
      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}"
      post :facebook_connect, {:facebook_token => 'foo', :use_route => :tregg_oauth2_rails}
      response.status.should == 412
      JSON.parse(response.body)['error'].should == 'It was not possible to connect the Facebook account'
    end

    it 'fails if the FB account is already in use' do
      another_user = create(:user)
      another_user.facebook_connect(@fb_user['access_token'])
      another_user.save!
      request.env['HTTP_AUTHORIZATION'] = "OAuth #{@token}"
      post :facebook_connect, {:facebook_token => @fb_user['access_token'], :use_route => :tregg_oauth2_rails}

      response.status.should == 412
      JSON.parse(response.body)['error'].should == 'Facebook account already in use'
    end
  end
end
