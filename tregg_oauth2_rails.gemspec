$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "tregg_oauth2_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "tregg_oauth2_rails"
  s.version     = TreggOauth2Rails::VERSION
  s.authors     = ['Matteo Centenaro']
  s.email       = ['bugant@gmail.com']
  s.homepage    = 'https://bitbucket.org/fungostudios/tregg_oauth2_rails'
  s.summary     = 'Tregg OAuth2 provider'
  s.description = 'tregg_oauth2_rails is a mountable Rails engine implementing an OAuth2 provider'

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency 'rails', '~> 4.0.0'
  s.add_dependency 'rails-api'
  s.add_dependency 'active_model_serializers'
  s.add_dependency 'songkick-oauth2-provider' # NOTE: use git://github.com/bugant/oauth2-provider.git upstream-master branch
  s.add_dependency 'koala'

  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'debugger'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'shoulda-matchers'
  s.add_development_dependency 'factory_girl_rails'
  s.add_development_dependency 'rspec_api_documentation'
end
