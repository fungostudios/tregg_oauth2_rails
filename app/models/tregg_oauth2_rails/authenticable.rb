module TreggOauth2Rails
  module Authenticable
    def self.included(klass)
      modules = [Songkick::OAuth2::Model::ResourceOwner,
        TreggOauth2Rails::PasswordAuthenticable,
        TreggOauth2Rails::FacebookAuthenticable
      ]

      modules.each {|m| klass.send(:include, m)}
    end
  end
end
