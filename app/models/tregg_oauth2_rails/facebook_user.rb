require 'koala'

module TreggOauth2Rails
  class FacebookUser
    def initialize(token)
      @token = token
      @graph = Koala::Facebook::API.new(token)
    end

    def me
      @graph.get_object('me')
    end
  end
end
