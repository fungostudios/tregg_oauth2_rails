require 'bcrypt'

module TreggOauth2Rails
  module PasswordAuthenticable
    MIN_PASSWORD_LENGTH = 6
    def self.included(klass)
      klass.send(:include, BCrypt)
      klass.extend(ClassMethods)
    end

    module ClassMethods
      def by_recovery_token(token, expiration = nil)
        u = find_by_reset_pwd_token(token)
        expiration ||= 1.hours.ago
        if !u.nil? && (u.password_reset_sent_at < expiration)
          u = nil
        end

        u
      end
    end

    def password
      BCrypt::Password.new(read_attribute(:password)) if !read_attribute(:password).blank?
    end

    def password=(new_password)
      raise StandardError.new('Invalid password') if new_password.blank?
      raise StandardError.new("Password must be at least #{MIN_PASSWORD_LENGTH} chars long") if new_password.length < MIN_PASSWORD_LENGTH
      write_attribute(:password, BCrypt::Password.create(new_password))
    end

    def password_authenticate?(password)
      self.password == password
    end

    def recover_password_token!(retries = 5)
      begin
        self.reset_pwd_token = SecureRandom.urlsafe_base64
        self.password_reset_sent_at = Time.now
        save!
      rescue ActiveRecord::RecordNotUnique => not_unique
        retries -= 1
        if retries > 0
          retry
        else
          raise not_unique
        end
      end

      self.reset_pwd_token
    end
  end
end
