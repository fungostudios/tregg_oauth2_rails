require 'date'

module TreggOauth2Rails
  module FacebookAuthenticable
    def self.included(klass)
      klass.extend(ClassMethods)
    end

    module ClassMethods
      def from_facebook(fb_token)
        fb_user = ::TreggOauth2Rails::FacebookUser.new(fb_token).me
        if !(user = ::TreggOauth2Rails.authenticable_class.find_by_facebook_uid(fb_user['id']))
          user = ::TreggOauth2Rails.authenticable_class.new
          user.facebook_data = fb_user
        end

        user
      end
    end

    def facebook_connect(fb_token)
      fb_user = TreggOauth2Rails::FacebookUser.new(fb_token).me
      self.facebook_data = fb_user
      self
    end

    def facebook_data=(fb_user)
      self.facebook_uid = fb_user['id']
      self.facebook_first_name = fb_user['first_name']
      self.facebook_last_name = fb_user['last_name']
      self.facebook_gender = fb_user['gender']
      self.facebook_birthday = DateTime.parse(fb_user['birthday']).to_time if fb_user['birthday']
      self.facebook_username = fb_user['username']
      self.username ||= fb_user['username']
      self.facebook_email = fb_user['email']
      self.facebook_picture = "http://graph.facebook.com/#{fb_user['id']}/picture"
    end
  end
end
