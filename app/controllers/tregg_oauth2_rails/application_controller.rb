module TreggOauth2Rails
  class ApplicationController < ActionController::API
    include TreggOauth2Rails::AuthenticationFilters
    before_action :authenticate!
    before_action :set_access_control_headers

    def set_access_control_headers
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Content-Disposition'

      head(:ok) if request.request_method == "OPTIONS"
    end
  end
end
