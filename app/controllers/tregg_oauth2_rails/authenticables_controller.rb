require_dependency 'tregg_oauth2_rails/application_controller'

module TreggOauth2Rails
  class AuthenticablesController < ApplicationController
    skip_before_action :authenticate!, :only => [:create, :sign_up]

    def create
      oauth2 = Songkick::OAuth2::Provider.parse(nil, request.env)
      oauth2.response_headers.each_pair {|header, value| response.headers[header] = value}
      body = oauth2.response_body
      if body.nil? && !oauth2.valid?
        body = {:error => oauth2.error, :error_description => oauth2.error_description}
      end

      render :json => body, :status => oauth2.response_status
    end

    def sign_up
      sparams = sign_up_params
      required_params.each do |param|
        if sparams[param].blank?
          render :json => {:error => "You must provide #{required_params.join(', ')}"}, :status => 400
          return
        end
      end

      client = Songkick::OAuth2::Model::Client.where(:client_id => sparams[:client_id]).first
      if client.nil?
        render :json => {:error => 'Client App Not Found'}, :status => 400
        return
      end

      begin
        user = TreggOauth2Rails.authenticable_class.create!(:username => sparams[:username], :password => sparams[:password])
      rescue ActiveRecord::RecordNotUnique => not_unique
        logger.info 'SING-UP: Username already in use'
        logger.info "SIGN-UP: #{not_unique.message}"
        render :json => {:error => 'Username already in use'}, :status => 412
        return
      rescue => err
        logger.error "SIGN-UP: #{err.message}"
        render :json => {:error => err.message}, :status => 412
        return
      end

      token = Songkick::OAuth2::Model::Authorization.for(user, client).generate_access_token

      render :json => {:user => user, :access_token => token}
    end

    def sign_out
      token = TreggOauth2Rails.access_token(current_user, [], request)
      token.authorization.update_attribute(:access_token, nil)

      render :json => {:success => 'ok'}
    end

    def show
      render :json => {:user => current_user, :role => current_user.role}
    end

    private

    def required_params
      [:client_id, :username, :password, :client_id]
    end

    def sign_up_params
      params.permit(*required_params)
    end
  end
end
