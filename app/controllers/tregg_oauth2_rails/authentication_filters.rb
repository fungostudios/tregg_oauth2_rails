module TreggOauth2Rails
  module AuthenticationFilters
    private

    def authenticate!
      auth = TreggOauth2Rails.access_token(:implicit, [], request)
      response.headers.merge!(auth.response_headers)
      response.status = auth.response_status

      if !auth.valid?
        render :json => TreggOauth2Rails::AUTH_ERROR_RESPONSE
      end

      @user = auth.owner
    end

    def current_user
      @user
    end
  end
end
