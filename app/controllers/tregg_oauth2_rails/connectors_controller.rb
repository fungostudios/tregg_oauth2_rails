require_dependency 'tregg_oauth2_rails/application_controller'

module TreggOauth2Rails
  class ConnectorsController < ApplicationController
    def facebook_connect
      begin
        facebook_params
      rescue ActionController::ParameterMissing => param_error
        render :json => {:error => param_error.message}, :status => 400
        return
      end

      if !current_user.facebook_uid.blank?
        render :json => {:error => 'Already connected to Facebook', :error_description => 'Your account is already linked to Facebook'}, :status => 400
        return
      end

      begin
        current_user.facebook_connect(params[:facebook_token])
        current_user.save!
      rescue ActiveRecord::RecordNotUnique => not_unique
        render :json => {:error => 'Facebook account already in use', :error_description => 'This Facebook account is already linked to another user'}, :status => 412
        return
      rescue => err
        render :json => {:error => 'It was not possible to connect the Facebook account', :error_description => err.message}, :status => 412
        return
      end

      render :json => current_user.reload
    end

    private

    def facebook_params
      params.require(:facebook_token)
    end
  end
end
