Songkick::OAuth2::Provider.handle_assertions 'https://graph.facebook.com' do |client, assertion|
  begin
    account = TreggOauth2Rails.authenticable_class.from_facebook(assertion)
    account.save!
    account.grant_access!(client)
  rescue
    nil
  end
end
