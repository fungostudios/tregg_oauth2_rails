Rails.application.config.filter_parameters += [:password]

Songkick::OAuth2::Provider.handle_passwords do |client, username, password|
  user = TreggOauth2Rails.authenticable_class.find_by_username(username)
  if user && user.password_authenticate?(password)
    user.grant_access!(client)
  else
    nil
  end
end
