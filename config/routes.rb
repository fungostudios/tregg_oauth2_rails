TreggOauth2Rails::Engine.routes.draw do
  post '/oauth/authorize', :to => 'authenticables#create', :as => 'authorize'
  post '/sign-up', :to => 'authenticables#sign_up', :as => 'sign_up'
  post '/sign-out', :to => 'authenticables#sign_out', :as => 'sign_out'
  post '/me/facebook_connect', :to => 'connectors#facebook_connect', :as => 'facebook_connect'
  get '/me', :to => 'authenticables#show', :as => 'me'
end
