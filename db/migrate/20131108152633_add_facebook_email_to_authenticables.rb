class AddFacebookEmailToAuthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    add_column table, :facebook_email, :string
  end
end
