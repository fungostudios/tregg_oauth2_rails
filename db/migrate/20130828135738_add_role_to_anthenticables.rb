class AddRoleToAnthenticables < ActiveRecord::Migration
  def change
    table = TreggOauth2Rails.authenticable_table
    add_column table, :role, :string
  end
end
